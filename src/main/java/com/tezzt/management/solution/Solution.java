package com.tezzt.management.solution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import com.tezzt.management.exercice.Exercice;
import com.tezzt.management.file.File;
import com.tezzt.management.user.User;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Solution {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name="EXERCICE_ID")
    private Exercice exercice;
    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;
    @OneToOne
    private File file;
    private double score;
    private String message;

}
