package com.tezzt.management.solution;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SolutionController {
    private final SolutionService solutionService;

    public SolutionController(SolutionService solutionService) {
        this.solutionService = solutionService;
    }

    @GetMapping("/solutions")
    public List<Solution> fetchSolutions() {
        return solutionService.getSolutions();
    }

    @GetMapping("/solution/{id}")
    public Solution fetchSolution(@PathVariable("id") Long id) {
        return solutionService.getSolution(id);
    }

    @PostMapping("/solution")
    public Long addSolution(@RequestBody Solution solution) {
        return solutionService.add(solution).getId();
    }

    @PostMapping("/solution/{id}")
    public Long editSolution(@RequestBody Solution solution, @PathVariable("id") Long id) {
        return solutionService.edit(id, solution);
    }

    @DeleteMapping("/solution/{id}")
    public void deleteSolution(@PathVariable("id") Long id) {
        solutionService.delete(id);
    }


    @PostMapping("/solution/{id}/file/{idFile}/add")
    public void addFile(@PathVariable("idFile") Long idFile, @PathVariable("id") Long id) {
        solutionService.addFile(id, idFile);
    }

    @PostMapping("/solution/{id}/file/{idFile}/delete")
    public void deleteFile(@PathVariable("idFile") Long idFile, @PathVariable("id") Long id) {
        solutionService.deleteFile(id, idFile);
    }
}
