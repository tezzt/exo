package com.tezzt.management.solution;

import org.springframework.data.repository.CrudRepository;

import com.tezzt.management.exercice.Exercice;

import java.util.List;

public interface SolutionRepository extends CrudRepository<Solution, Long> {
}
