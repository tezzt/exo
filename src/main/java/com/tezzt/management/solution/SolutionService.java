package com.tezzt.management.solution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tezzt.management.exercice.ExerciceRepository;
import com.tezzt.management.file.File;
import com.tezzt.management.file.FileRepository;

import java.util.List;

@Service
public class SolutionService {
	
	@Autowired
    private SolutionRepository solutionRepository;
	
	@Autowired
    private ExerciceRepository exerciceRepository;
	
	@Autowired
    private FileRepository fileRepository;
	
	@Autowired
	private RestTemplate restTemplate;


    public Solution add(Solution solution){
        Solution s = solutionRepository.save(solution);
        return s;
    }

    public List<Solution> getSolutions(){
        return (List<Solution>) solutionRepository.findAll();
    }

    public Solution getSolution(Long id){
        return solutionRepository.findById(id).orElse(new Solution());
    }

    public Long edit(Long id, Solution solution) {
        if (solutionRepository.findById(id).isPresent()) {
            solution.setId(id);
            return solutionRepository.save(solution).getId();
        }
        else return Long.valueOf(-1);
    }

    public void delete(Long id) {
        if (solutionRepository.findById(id).isPresent())
            solutionRepository.delete(solutionRepository.findById(id).get());
    }

    public void addFile(Long id, Long idFile){
    	Solution solution = solutionRepository.findById(id).get();
    	File file = fileRepository.findById(idFile).get();
        if (file != null && solution != null) {
            file.setSolution(solution);
            fileRepository.save(file);
            solution.setFile(file);
            SolutionForJudgeDTO dto = SolutionFactory.CreateDto(solution);
            Resultat resultat = restTemplate.postForObject("http://judge-service/solutions/", dto, Resultat.class);
            solution.setScore(resultat.getScore());
            solution.setMessage(resultat.getMsg());
            solutionRepository.save(solution);

        }
    }

    public void deleteFile(Long id, Long idFile){
        if (exerciceRepository.findById(id).isPresent() && fileRepository.findById(idFile).isPresent()) {
            fileRepository.findById(idFile).get().setSolution(null);
            fileRepository.save(fileRepository.findById(idFile).get());
        }
    }

}
