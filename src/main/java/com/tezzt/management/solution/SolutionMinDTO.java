package com.tezzt.management.solution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolutionMinDTO {
    private String userFirstNmae;
    private String userLastName;
    private int score;
}
