package com.tezzt.management.solution;


public class SolutionFactory {

	public static SolutionForJudgeDTO CreateDto(Solution s) {
		return new SolutionForJudgeDTO(
				new Long(s.getId()), 
				s.getFile().getPath(), 
				s.getExercice().getFile().getPath()
		);
	}
}
