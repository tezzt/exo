package com.tezzt.management.tag;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tezzt.management.exercice.Exercice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    private String name;
    @ManyToMany(mappedBy = "tags")
    @JsonBackReference
    private List<Exercice> exercices = new ArrayList<>();
}
