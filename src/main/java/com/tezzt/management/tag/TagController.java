package com.tezzt.management.tag;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TagController {
    private final TagService tagService;
    private final TagRepository tagRepository;

    public TagController(TagService tagService, TagRepository tagRepository) {
        this.tagService = tagService;
        this.tagRepository = tagRepository;
    }

    @GetMapping("/tags")
    public List<Tag> fetchTags() {
        return tagService.getTags();
    }

    @GetMapping("/tag/{id}")
    public Tag fetchTag(@PathVariable("id") Long id) {
        return tagService.getTag(id);
    }

    @PostMapping("/tag")
    public Long addTag(@RequestBody Tag tag) {
        return tagService.add(tag).getId();
    }

    @PostMapping("/tag/{id}")
    public Long editTag(@RequestBody Tag tag, @PathVariable("id") Long id) {
        return tagService.edit(id, tag);
    }

    @DeleteMapping("/tag/{id}")
    public void deleteTag(@PathVariable("id") Long id) {
        tagService.delete(id);
    }
}
