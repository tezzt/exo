package com.tezzt.management.tag;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    public Tag add(Tag tag){
        return tagRepository.save(tag);
    }

    public List<Tag> getTags(){
        return (List<Tag>) tagRepository.findAll();
    }

    public Tag getTag(Long id){
        return tagRepository.findById(id).orElse(new Tag());
    }

    public Long edit(Long id, Tag tag) {
        if (tagRepository.findById(id).isPresent()) {
            tag.setId(id);
            return tagRepository.save(tag).getId();
        }
        else return Long.valueOf(-1);
    }

    public void delete(Long id) {
        if (tagRepository.findById(id).isPresent())
            tagRepository.delete(tagRepository.findById(id).get());
    }
}
