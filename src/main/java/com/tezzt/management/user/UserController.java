package com.tezzt.management.user;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;

    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping("/useres")
    public List<User> fetchUsers() {
        return userService.getUsers();
    }

    @GetMapping("/user/{id}")
    public User fetchUser(@PathVariable("id") Long id) {
        return userService.getUser(id);
    }

    @PostMapping("/user")
    public Long addUser(@RequestBody User user) {
        return userService.add(user).getId();
    }

    @PostMapping("/user/{id}")
    public Long editUser(@RequestBody User user, @PathVariable("id") Long id) {
        return userService.edit(id, user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @PostMapping("/user/{id}/exercice/{idExo}/add")
    public void addExo(@PathVariable("idExo") Long idExo, @PathVariable("id") Long id) {
        userService.addExo(id, idExo);
    }

    @PostMapping("/user/{id}/exercice/{idExo}/delete")
    public void deleteExo(@PathVariable("id") Long id, @PathVariable("idExo") Long idExo) {
        userService.deleteExo(id, idExo);
    }

    @PostMapping("/user/{id}/solution/{idSol}/add")
    public void addSolution(@PathVariable("idSol") Long idSol, @PathVariable("id") Long id) {
        userService.addSolution(id, idSol);
    }

    @PostMapping("/user/{id}/solution/{idSol}/delete")
    public void deleteSolution(@PathVariable("idSol") Long idSol, @PathVariable("id") Long id) {
        userService.deleteSolution(id, idSol);
    }
    
}
