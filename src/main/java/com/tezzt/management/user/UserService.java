package com.tezzt.management.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.*;

import com.tezzt.management.exercice.Exercice;
import com.tezzt.management.exercice.ExerciceRepository;
import com.tezzt.management.solution.SolutionRepository;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ExerciceRepository exerciceRepository;
    private SolutionRepository solutionRepository;

    public UserService(UserRepository userRepository, ExerciceRepository exerciceRepository, SolutionRepository solutionRepository) {
        this.userRepository = userRepository;
        this.exerciceRepository = exerciceRepository;
        this.solutionRepository = solutionRepository;
    }

    public User add(User user){
        return userRepository.save(user);
    }

    public List<User> getUsers(){
        return (List<User>) userRepository.findAll();
    }

    public User getUser(Long id){
        return userRepository.findById(id).orElse(new User());
    }

    public Long edit(Long id, User user) {
        if (userRepository.findById(id).isPresent()) {
            user.setId(id);
            return userRepository.save(user).getId();
        }
        else return Long.valueOf(-1);
    }

    public void delete(Long id) {
        if (userRepository.findById(id).isPresent())
            userRepository.delete(userRepository.findById(id).get());
    }

    public void addExo(Long id, Long idExo) {
        if (userRepository.findById(id).isPresent() && exerciceRepository.findById(idExo).isPresent()) {
            exerciceRepository.findById(idExo).get().setUser(userRepository.findById(id).get());
            exerciceRepository.save(exerciceRepository.findById(idExo).get());
        }

    }

    public void deleteExo(Long id, Long idExo) {
        if (userRepository.findById(id).isPresent() && exerciceRepository.findById(idExo).isPresent()) {
            exerciceRepository.findById(idExo).get().setUser(null);
            exerciceRepository.save(exerciceRepository.findById(idExo).get());
        }
    }

    public void addSolution(Long id, Long idSol) {
        if (solutionRepository.findById(idSol).isPresent() && userRepository.findById(id).isPresent()) {
            solutionRepository.findById(idSol).get().setUser(userRepository.findById(id).get());
            solutionRepository.save(solutionRepository.findById(idSol).get());
        }
    }

    public void deleteSolution(Long id, Long idSol) {
        if (solutionRepository.findById(id).isPresent() && userRepository.findById(id).isPresent()) {
            solutionRepository.findById(idSol).get().setUser(null);
            solutionRepository.save(solutionRepository.findById(idSol).get());
        }
    }

}
