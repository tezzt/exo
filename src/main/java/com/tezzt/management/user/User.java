package com.tezzt.management.user;

import com.tezzt.management.exercice.Exercice;
import com.tezzt.management.solution.Solution;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String username;
    private String role; 
    @OneToMany(mappedBy = "user", targetEntity = Exercice.class)
    private List<Exercice> exercices = new ArrayList<>();
    @OneToMany(mappedBy = "user", targetEntity = Solution.class)
    private List<Solution> solutions = new ArrayList<>();
}
