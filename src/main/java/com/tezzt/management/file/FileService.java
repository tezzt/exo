package com.tezzt.management.file;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.List;

@Service
public class FileService {
    private FileRepository fileRepository;

    public FileService(FileRepository fileRepository){
        this.fileRepository = fileRepository;
    }

    public File add(File file){
        return fileRepository.save(file);
    }

    public List<File> getFiles(){
        return (List<File>) fileRepository.findAll();
    }

    public File getFile(Long id){
        return fileRepository.findById(id).orElse(new File());
    }

    public Long edit(Long id, File file) {
        if (fileRepository.findById(id).isPresent()) {
            file.setId(id);
            return fileRepository.save(file).getId();
        }
        else return Long.valueOf(-1);
    }

    public void delete(Long id) {
        if (fileRepository.findById(id).isPresent())
            fileRepository.delete(fileRepository.findById(id).get());
    }

    public String hash(String file) throws Exception{
        StringBuilder sb = new StringBuilder();
        MessageDigest md = MessageDigest.getInstance("MD5");
        try(DigestInputStream dis = new DigestInputStream(new FileInputStream(file), md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
            byte[] hashInBytes = md.digest();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOEXECPTION CAUGHT");
        }
        return sb.toString();
    }
}
