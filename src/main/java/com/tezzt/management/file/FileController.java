package com.tezzt.management.file;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.tezzt.management.upload.StorageService;

import java.io.IOException;
import java.util.List;

@RestController
public class FileController {
    private final FileService fileService;
    private final FileRepository fileRepository;
    private StorageService storageService;

    public FileController(FileService fileService, FileRepository fileRepository, StorageService storageService) {
        this.fileService = fileService;
        this.fileRepository = fileRepository;
        this.storageService = storageService;
    }

    @GetMapping("/files")
    public List<File> fetchFiles() {
        return fileService.getFiles();
    }

    @GetMapping("/file/{id}")
    public File fetchFile(@PathVariable("id") Long id) {
        return fileService.getFile(id);
    }

    @PostMapping("/uploadfile")
    public Long addFile(@RequestParam("file") MultipartFile file) {
        File f = new File();
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            storageService.store(file);
            System.out.println("Passed store");
            f.setHash(fileService.hash(storageService.getPath()+"\\"+filename));
            f.setPath(storageService.getPath() + "\\" + filename);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return fileService.add(f).getId();
    }

    @PostMapping("/file/{id}")
    public Long editFile(@RequestBody File file, @PathVariable("id") Long id) {
        return fileService.edit(id, file);
    }

    @DeleteMapping("/file/{id}")
    public void deleteFile(@PathVariable("id") Long id) {
        fileService.delete(id);
    }
}
