package com.tezzt.management.file;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tezzt.management.exercice.Exercice;
import com.tezzt.management.solution.Solution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class File {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String path;
    private String hash;
    @OneToOne(mappedBy="file")
    @JsonBackReference
    private Exercice exercice;
    @OneToOne(mappedBy="file")
    private Solution solution;
}
