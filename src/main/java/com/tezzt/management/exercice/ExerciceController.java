package com.tezzt.management.exercice;

import org.springframework.web.bind.annotation.*;

import com.tezzt.management.exercice.Exercice;
import com.tezzt.management.exercice.ExerciceRepository;
import com.tezzt.management.user.UserService;

import java.util.List;

@RestController
public class ExerciceController {
    private final ExerciceService exerciceService;

    public ExerciceController(ExerciceService exerciceService) {
        this.exerciceService = exerciceService;
    }

    @GetMapping("/exercices")
    public List<Exercice> fetchExercices() {
        return exerciceService.getExercices();
    }

    @GetMapping("/exercice/{id}")
    public Exercice fetchExercice(@PathVariable("id") Long id) {
        return exerciceService.getExercice(id);
    }

    @PostMapping("/exercice")
    public Long addExercice(@RequestBody Exercice exercice) {
        return exerciceService.add(exercice).getId();
    }

    @PostMapping("/exercice/{id}")
    public Long editExercice(@RequestBody Exercice exercice, @PathVariable("id") Long id) {
        return exerciceService.edit(id, exercice);
    }

    @DeleteMapping("/exercice/{id}")
    public void deleteExercice(@PathVariable("id") Long id) {
        exerciceService.delete(id);
    }

    @PostMapping("/exercice/{id}/solution/{idSol}/add")
    public void addSolution(@PathVariable("idSol") Long idSol, @PathVariable("id") Long id) {
        exerciceService.addSolution(id, idSol);
    }

    @PostMapping("/exercice/{id}/user/{idUsr}/add")
    public void addUser(@PathVariable("idUsr") Long idUsr, @PathVariable("id") Long id) {
        exerciceService.addUser(id, idUsr);
    }

    @PostMapping("/exercice/{id}/solution/{idSol}/delete")
    public void deleteSolution(@PathVariable("idSol") Long idSol, @PathVariable("id") Long id) {
        exerciceService.deleteSolution(id, idSol);
    }

    @PostMapping("/exercice/{id}/file/{idFile}/add")
    public void addFile(@PathVariable("idFile") Long idFile, @PathVariable("id") Long id) {
        exerciceService.addFile(id, idFile);
    }

    @PostMapping("/exercice/{id}/file/{idFile}/delete")
    public void deleteFile(@PathVariable("idFile") Long idFile, @PathVariable("id") Long id) {
        exerciceService.deleteFile(id, idFile);
    }

    @GetMapping("/exercice/{id}/stats")
    public ExerciceResultsDTO exerciceStats(@PathVariable("id") Long id) {
        return exerciceService.calculateStats(id);
    }
    
}
