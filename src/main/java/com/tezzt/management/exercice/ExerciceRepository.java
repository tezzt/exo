package com.tezzt.management.exercice;

import org.springframework.data.repository.CrudRepository;

public interface ExerciceRepository extends CrudRepository<Exercice, Long> {
}
