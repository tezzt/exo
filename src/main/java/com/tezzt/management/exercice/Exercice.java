package com.tezzt.management.exercice;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tezzt.management.file.File;
import com.tezzt.management.solution.Solution;
import com.tezzt.management.tag.Tag;
import com.tezzt.management.user.User;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Exercice {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    private String name;
    private String technology;
    private String description;
    private String difficulty;
    private Date deadline;
    private Date disponibility;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID")
    @JsonBackReference
    private User user;
    @OneToMany
    private List<Solution> solutions = new ArrayList<>();
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="FILE_ID")
    private File file;
    @ManyToMany
    @JoinTable( name="EXERCICE_TAG", joinColumns=@JoinColumn(name="EXERCICE_ID", referencedColumnName="ID"),
            inverseJoinColumns=@JoinColumn(name="TAG_ID", referencedColumnName="ID"))
    private List<Tag> tags = new ArrayList<>();

    /*public boolean equals(@NotNull Exercice exercice){
        return exercice.id.equals(id);
    }*/
}
