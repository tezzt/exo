package com.tezzt.management.exercice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

import com.tezzt.management.solution.SolutionMinDTO;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExerciceResultsDTO {
    private String name;
    private String technology;
    private String desc;
    private String difficulty;
    private Date deadline;
    private Date dispo;
    private List<SolutionMinDTO> solutionDTOList;
    private double mean;
    private double highestScore;
    private double lowestScore;
    private List<Double> scoreDistribution;
}
