package com.tezzt.management.exercice;
import org.springframework.stereotype.Service;

import com.tezzt.management.file.FileRepository;
import com.tezzt.management.solution.Solution;
import com.tezzt.management.solution.SolutionRepository;
import com.tezzt.management.user.User;
import com.tezzt.management.user.UserRepository;

import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ExerciceService {
    private final ExerciceRepository exerciceRepository;
    private final SolutionRepository solutionRepository;
    private final UserRepository userRepository;
    private FileRepository fileRepository;

    public ExerciceService(ExerciceRepository exerciceRepository, SolutionRepository solutionRepository, FileRepository fileRepository, UserRepository userRepository) {
        this.exerciceRepository = exerciceRepository;
        this.solutionRepository = solutionRepository;
        this.fileRepository = fileRepository;
        this.userRepository = userRepository;
    }

    public Exercice add(Exercice exercice){
        return exerciceRepository.save(exercice);
    }

    public List<Exercice> getExercices(){
        return (List<Exercice>) exerciceRepository.findAll();
    }

    public Exercice getExercice(Long id){
        return exerciceRepository.findById(id).orElse(new Exercice());
    }

    public Long edit(Long id, Exercice exercice) {
        if (exerciceRepository.findById(id).isPresent()) {
            exercice.setId(id);
            return exerciceRepository.save(exercice).getId();
        }
        else return Long.valueOf(-1);
    }

    public void delete(Long id) {
        if (exerciceRepository.findById(id).isPresent())
            exerciceRepository.delete(exerciceRepository.findById(id).get());
    }

    public void addSolution(Long id, Long idSol) {
        if (solutionRepository.findById(idSol).isPresent() && exerciceRepository.findById(id).isPresent()) {
            solutionRepository.findById(idSol).get().setExercice(exerciceRepository.findById(id).get());
            solutionRepository.save(solutionRepository.findById(idSol).get());
        }

    }

    public void addUser(Long id, Long idUsr) {
        if (userRepository.findById(idUsr).isPresent() && exerciceRepository.findById(id).isPresent()) {
            User usr = userRepository.findById(idUsr).get();
            Exercice exo = exerciceRepository.findById(id).get();
            exo.setUser(usr);
            exerciceRepository.save(exo);
        }

    }

    public void deleteSolution(Long id, Long idSol) {
        if (solutionRepository.findById(idSol).isPresent() && exerciceRepository.findById(id).isPresent()) {
            solutionRepository.findById(idSol).get().setExercice(null);
            solutionRepository.save(solutionRepository.findById(idSol).get());
        }
    }

    public void addFile(Long id, Long idFile){
        if (exerciceRepository.findById(id).isPresent() && fileRepository.findById(idFile).isPresent()) {
            fileRepository.findById(idFile).get().setExercice(exerciceRepository.findById(id).get());
            fileRepository.save(fileRepository.findById(idFile).get());
        }
    }

    public void deleteFile(Long id, Long idFile){
        if (exerciceRepository.findById(id).isPresent() && fileRepository.findById(idFile).isPresent()) {
            fileRepository.findById(idFile).get().setExercice(null);
            fileRepository.save(fileRepository.findById(idFile).get());
        }
    }

    public ExerciceResultsDTO calculateStats(Long id){
        Exercice exo = exerciceRepository.findById(id).orElse(new Exercice());
        ModelMapper modelMapper = new ModelMapper();
        if (!exo.getName().isEmpty()){
            ExerciceResultsDTO exoResults = modelMapper.map(exo, ExerciceResultsDTO.class);
            exoResults.setMean(calculateMean(exo.getSolutions()));
            exoResults.setHighestScore(getHighestScore(exo.getSolutions()));
            exoResults.setLowestScore(getLowestScore(exo.getSolutions()));
            exoResults.setScoreDistribution(generateRangeGraph(exo.getSolutions()));
            return exoResults;
        }
        return null;
    }

    private double calculateMean(@NotNull List<Solution> solutions){
        int sum = 0;
        for (Solution s: solutions){
            sum += s.getScore();
        }
        return (double)sum/solutions.size();
    }

    private double getHighestScore(@NotNull List<Solution> solutions){
    	double hs = 0;
        for (Solution s: solutions) {
            if( s.getScore() > hs) hs = s.getScore();
        }
        return hs;
    }

    private double getLowestScore(@NotNull List<Solution> solutions){
    	double ls = 100;
        for (Solution s: solutions) {
            if( s.getScore() < ls) ls = s.getScore();
        }
        return ls;
    }

    private List<Double> generateRangeGraph(@NotNull List<Solution> solutions){
        List<Double> result = Arrays.asList(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        for(Solution solution : solutions){
            int id = (int) (Math.floor(solution.getScore()) / 10);
            if(id == 10){
                id = 9;
            }
            result.set(id, result.get(id) + 1);
        }

        return result;
    }
}
